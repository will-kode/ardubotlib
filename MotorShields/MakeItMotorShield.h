#ifndef MAKE_IT_MOTOR_SHIELD_H
#define MAKE_IT_MOTOR_SHIELD_H

#include "../Commons/Commons.h"
#include "MotorShield.h"

/**
 * author:  Troi
 * class:   MakeItMotorShield
 * description:
 *  The MakeItMotorShield class provides a simple interface for
 *  controlling motors that are connected to the Make:it motor
 *  shield, which comes with the Make:it Robotics Starter kit.
 *  MakeItMotorShield is dervied from the MotorShield class and
 *  implements all of its methods.
 *
 * resources:
 *  - Make:it Robotics Starter Kit:
 *      http://www.makershed.com/products/makeit-robotics-start-kit
**/
class MakeItMotorShield : public MotorShield {
public:
    // constructor
    MakeItMotorShield();

    // deconstructor
    ~MakeItMotorShield();

    // this method should be called in the setup function
    virtual void begin();

    // sets a motor speed and direction on the specified port
    // speed range: -255 to 255
    // negative values = reverse; zero = stop; positive values = forward
    virtual void motorWrite(uint8_t port, int16_t speed);

private:
    // checks if the specified port is "valid"
    bool isValidPort(uint8_t port);

    // sends the motor commands
    void dcWrite(uint8_t type, uint8_t value);

private:
    // used to send or receive data to or from
    // the motor shield
    static const uint8_t kShieldSendHeader;
    static const uint8_t kShieldRecvHeader;

    // specifies to change the pwm (speed) of a
    // motor on a particular port
    static const uint8_t kMotorPwmSignal[];

    // specifies to change the direction of a
    // motor on a particular port
    static const uint8_t kMotorDirSignal[];

    // specifies the direction of the motor
    static const uint8_t kForwardDir;
    static const uint8_t kReverseDir;
};

#endif // end of MAKE_IT_MOTOR_SHIELD_H