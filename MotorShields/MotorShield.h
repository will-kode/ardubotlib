#ifndef MOTOR_SHIELD_H
#define MOTOR_SHIELD_H

#include "../Commons/Commons.h"

/**
 * author:  Troi
 * class:   MotorShield
 * description:
 *  MotorShield is an sbstract class that specifies a collection of
 *  methods that motor shields (in general) should have. These methods
 *  were derived from looking at different motor shields and their
 *  capabilites, which include intialization, activating ports,
 *  deactivating ports, and sending motor commands.
 *
**/
class MotorShield {
public:
    // constructor
    MotorShield();

    // deconstructor
    ~MotorShield();

    // initialization phase implementation in subclasses must
    // be called first
    virtual void begin() = 0;

    // sets a motor speed and direction on the specified port
    // speed can range from -255 to 255
    virtual void motorWrite(uint8_t port, int16_t speed) = 0;

protected:
    // checks if the specified port is "valid"
    virtual bool isValidPort(uint8_t port) = 0;

    // checks if the specified speed is "valid"
    bool isValidMotorSpeed(int16_t speed);

    // absolute value of the max speed
    static const int16_t kMaxSpeed;
};

#endif // end of MOTOR_SHIELD_H class