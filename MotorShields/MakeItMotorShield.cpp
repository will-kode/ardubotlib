#include "MakeItMotorShield.h"

const uint8_t MakeItMotorShield::kShieldSendHeader = 0x56;
const uint8_t MakeItMotorShield::kShieldRecvHeader = 0x76;

const uint8_t MakeItMotorShield::kMotorPwmSignal[] = { 0x80, 0x81, 0x82, 0x83 };

const uint8_t MakeItMotorShield::kMotorDirSignal[] = { 0x73, 0x74, 0x75, 0x76 };
const uint8_t MakeItMotorShield::kForwardDir = 0xff;
const uint8_t MakeItMotorShield::kReverseDir = 0x00;

MakeItMotorShield::MakeItMotorShield()
  : MotorShield()
{
    // no explicit implementation
}

MakeItMotorShield::~MakeItMotorShield() {
    // no explicit implementation
}

void MakeItMotorShield::begin() {
    // INITIALIZE PHASE:
    // open a connection to the motor shield
    Serial.begin(10420);
    delay(250);
}

void MakeItMotorShield::motorWrite(uint8_t port, int16_t speed) {
    if (isValidPort(port) && isValidMotorSpeed(speed)) {
        // get the port index
        uint8_t portIndex = port - 1;
        // set the direction of the motor (if the speed is not zero)
        if (speed != 0) {
            // if the speed is negative, signal that the motor should reverse
            // else, signal that the motor should go forward
            if (speed < 0) {
                dcWrite(kMotorDirSignal[portIndex], kReverseDir);
                speed = abs(speed);
            } else {
                dcWrite(kMotorDirSignal[portIndex], kForwardDir);
            }
        }
        // send the speed value
        dcWrite(kMotorPwmSignal[portIndex], speed);
    }
}

bool MakeItMotorShield::isValidPort(uint8_t port) {
    return (0 < port && port <= 4);
}

void MakeItMotorShield::dcWrite(uint8_t type, uint8_t value) {
    Serial.write(kShieldSendHeader);
    Serial.write(type);
    Serial.write(value);
    delay(40);
}
