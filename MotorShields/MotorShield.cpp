#include "MotorShield.h"

const int16_t MotorShield::kMaxSpeed = 255;

MotorShield::MotorShield() {
    // no explicit implementation
}

MotorShield::~MotorShield() {
    // no explicit implementation
}

bool MotorShield::isValidMotorSpeed(int16_t speed) {
    if (-kMaxSpeed <= speed && speed <= kMaxSpeed)
        return true;
    else
        return false;
}
