#include "LED.h"

LED::LED()
  : ledPin(kInvalidPin),
    ledIsOn(false),
    pulseWidth(0)
{
    // no explicit implementation
}

LED::~LED() {
    // no explicit implementation
}

void LED::attach(uint8_t pin) {
    if (isValidPin(pin)) {
        // sets the output pin and pin mode as output
        ledPin = pin;
        pinMode(ledPin, OUTPUT);
    }
}

bool LED::isAttached() const {
    return (ledPin != kInvalidPin);
}

void LED::detach() {
    // turn off the LED
    off();
    // set the LED pin variable to an invalid number
    ledPin = kInvalidPin;
}

bool LED::isOn() const {
    return ledIsOn;
}

void LED::on() {
    // update the LED's status
    updateLEDState(true);
}

void LED::off() {
    // update the LED's status
    updateLEDState(false);
}

void LED::setBrightness(uint8_t value) {
    // constrain the value between 0 and kMaxPWM (255)
    // if the value was above kMaxPWM (value > 255), then ledValue = 255
    // if the value was below 0 (value < 0), the ledValue = 0
    uint8_t ledValue = constrain(value, 0, kMaxPWM);
    pulseWidth = ledValue;

    // according to the Arduino doc, PWM values of 10 or below with some
    // pins result in the LED being off.
    // reference -> http://arduino.cc/en/Reference/AnalogWrite
    if (ledValue <= 10)
        updateLEDState(false);
    else
        updateLEDState(true);
}

void LED::toggle() {
    if (isOn())
        off();
    else
        on();
}

void LED::updateLEDState(bool turnOnLED) {
    // check if the LED was set before attempting to update its status
    if (isAttached()) {
        // check the attached pin is digital with PWM
        if (pinHasPWM(ledPin)) {
            // check whether the LED should be on or off
            if (turnOnLED)
                analogWrite(ledPin, pulseWidth);
            else
                analogWrite(ledPin, 0);
        }
        // else it is attached to a digital pin WITHOUT PWM
        else {
            // check whether the LED should be on or off
            if (turnOnLED)
                digitalWrite(ledPin, HIGH);
            else
                digitalWrite(ledPin, LOW);
        }
        // update the "ledIsOn" boolean variable
        ledIsOn = turnOnLED;
    }
}
