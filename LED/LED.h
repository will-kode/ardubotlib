#ifndef LED_H
#define LED_H

#include "../Commons/Commons.h"

/**
 * author:  Troi
 * class:   LED
 * description:
 *  The LED class provides a simple interface for operating a single LED.
 *
 * resources:
 *  - Arduino Pulse Width Modulation: http://arduino.cc/en/Tutorial/PWM
 *  - Arduino reference page: http://arduino.cc/en/Reference/HomePage
**/
class LED {
public:
    // constructor
    LED();
    
    // deconstructor
    ~LED();

    // attaches (sets) the output pin and its type
    // * NOTE: should be called within the setup function
    void attach(uint8_t pin);

    // detaches the LED from the output pin.
    // the LED is turned off and 
    void detach();

    // specifies if the LED instance was attached to a pin
    bool isAttached() const;

    // specifies if the LED is on
    bool isOn() const;

    // sets the LED brightness to zero
    void off();

    // sets the LED brightness to max
    void on();

    // sets the "brightness level" for the LED
    void setBrightness(uint8_t value);

    // switches the LED between ON and OFF
    void toggle();

private:
    // updates the state of the LED
    void updateLEDState(bool turnOnLED);

private:
    // the pin the LED is attached to
    int8_t ledPin;
    // specifies if the LED is on (value is true) or off (value is false)
    bool ledIsOn;
    // the pulse width modulation for the LED
    uint8_t pulseWidth;
};

#endif // end of LED_H
