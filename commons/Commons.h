#ifndef COMMONS_H
#define COMMONS_H

#include "Arduino.h"

// the max pulse width modulation output
const static uint8_t kMaxPWM = 255u;
// a constant for denoting an invalid pin
const static int8_t kInvalidPin = -1;

// checks if the specified pin is "valid" (determined by the number of pins)
#if defined(NUM_DIGITAL_PINS)
#define isValidPin(pin)             (pin < NUM_DIGITAL_PINS)
#endif

// checks if the specified pin has a pulse modulation width option
#if defined(digitalPinHasPWM)
#define pinHasPWM(pin)              (digitalPinHasPWM(pin))
#endif

#endif // end of COMMONS_H