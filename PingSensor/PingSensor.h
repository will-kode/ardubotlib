#ifndef PING_SENSOR_H
#define PING_SENSOR_H

#include "../Commons/Commons.h"

/**
 * author:  Troi
 * class:   PingSensor
 * description:
 *  The PingSensor class procides an interface for employing an ultrasonic
 *  sensor.
 *
 * resources:
 *  - Ping: http://arduino.cc/en/Tutorial/Ping?from=Tutorial.UltrasoundSensor
 *  - NewPing: http://playground.arduino.cc/Main/UltrasonicSensor
 *    * Code from the Ping and NewPing libraries were used to build this class.
**/
class PingSensor {
  
public:
  // constructor
  PingSensor();
  
  // deconstructor
  ~PingSensor();

  // sets the pin used for BOTH the trigger and echo pins
  void attach(uint8_t pingPin);
  
  // sets the trigger and echo pins for the ultrasonic sensor, respectively
  void attach(uint8_t pingTriggerPin, uint8_t pingEchoPin);

  // detaches the ping sensor from the trigger and echo pins
  // the ping method will return zero if this method is called
  void detach();

  // determines if the ping sensor was "attached" to a pin
  bool isAttached() const;
  
  // computes the time an ultrasonic signal took to travel to the
  // nearest, direct-line-of-sight object, or returns zero if the pins
  // were not set or there was an error (such as a ping timeout)
  unsigned long ping();

  // converts microseconds to inches
  static unsigned long microsecsToIn(unsigned long microsecs);

  // converts microseconds to centimeters
  static unsigned long microsecsToCm(unsigned long microsecs);
  
private:
  // the Arduino pin that the sensor's trigger pin is connected to
  int8_t triggerPin;
  // the Arduino pin that the sensor's echo pin is connected to
  int8_t echoPin;
};

#endif // end of PING_SENSOR_H