#include "PingSensor.h"

PingSensor::PingSensor()
  : triggerPin(kInvalidPin),
    echoPin(kInvalidPin)
{ }

PingSensor::~PingSensor() {
    /* no explicit implementation */
}

void PingSensor::attach(uint8_t pingPin) {
    attach(pingPin, pingPin);
}

void PingSensor::attach(uint8_t pingTriggerPin, uint8_t pingEchoPin) {
    // determine if both pins are valid before they are set
    if (isValidPin(pingTriggerPin) && isValidPin(pingEchoPin)) {
        triggerPin = pingTriggerPin;
        echoPin = pingEchoPin;
    }
}

void PingSensor::detach() {
    triggerPin = echoPin = kInvalidPin;
}

unsigned long PingSensor::ping() {
    unsigned long duration = 0ul;
    // if the sensor cannot ping, then the duration will be zero
    if (isAttached()) {
        // send a low pulse to ensure a clear HIGH pulse
        pinMode(triggerPin, OUTPUT);
        digitalWrite(triggerPin, LOW);
        // wait, send a high pulse, then wait again
        delayMicroseconds(2);
        digitalWrite(triggerPin, HIGH);
        delayMicroseconds(5);
        // send a low pulse
        digitalWrite(triggerPin, LOW);
        // using the same pin, await the ping reflection
        pinMode(echoPin, INPUT);
        // return the time it took for the reflection to go and return
        duration = pulseIn(echoPin, HIGH) / 2;
    }
    return duration;
}

unsigned long PingSensor::microsecsToIn(unsigned long microsecs) {
    return microsecs / 74ul;
}

unsigned long PingSensor::microsecsToCm(unsigned long microsecs) {
    return microsecs / 29ul;
}

bool PingSensor::isAttached() const {
    return (triggerPin != kInvalidPin && echoPin != kInvalidPin);
}
