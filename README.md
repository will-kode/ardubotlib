![logo.png](https://bitbucket.org/repo/8KKMxM/images/3354009649-logo.png)
# ArdubotLib

This library can be used with an Arduino board (such as an UNO) to create simple autonomous sensors or robots. To do this, instructions to control components such as motor shields and ping sensors were abstracted so that developers can focus on designing the board's control algorithm.
