#include <LED.h>
#include <PingSensor.h>

// number of LEDs and the max number that can be represented for the number of LEDs
const uint8_t kNumbOfLEDs = 4;
const uint8_t kMaxBinaryNumber = pow(2, kNumbOfLEDs) - 1;

// LEDs
LED leds[kNumbOfLEDs];

// Ultrasonic sensor
PingSensor sonar;

void setup() {
  // LEDs from left to right where the left most is highest bit
  // green LED left
  leds[0].attach(8);
  // red LED left
  leds[1].attach(12);
  // green LED right
  leds[2].attach(13);
  // red LED right
  leds[3].attach(7);
  
  // attach the sonar to a pin
  sonar.attach(3);

  // initialize serial communication
  Serial.begin(9600);
}

void loop() {
  // determine the distance from the sonar to the nearest, line-of-sight obstacle
  long duration, distance;
  duration = sonar.ping();
  distance = PingSensor::microsecsToIn(duration);
  // print distance information
  Serial.print("distance = ");
  Serial.print(distance);
  Serial.print(" inches; ");
  // display distance in binary
  displayDistance(distance);
  // print a new line
  Serial.println();
  // delay for 0.2 seconds (loop five times a second)
  delay(200);
}

void displayDistance(long distance) {  
  if (distance > kMaxBinaryNumber || distance == 0) {
    Serial.print("blinking all");
    // alternate between turning off and on the LEDs (blinking)
    static boolean allLEDsOn = true;
    for (int i = 0; i < kNumbOfLEDs; i++) {
      if (allLEDsOn)
        leds[i].off();
      else
        leds[i].on();
    }
    // alternates between false and true
    allLEDsOn = (!allLEDsOn);
  }
  // distance is "within range"
  else {
    Serial.print("binary = ");
    for (int i = kNumbOfLEDs - 1; i >= 0; i++) {
      // determine if the LED should be off or on
      // - do the bit operation of anding the last bit of distance and a one.
      //   - if the result is one, then turn on the LED
      //   - else (the result was a zero), then turn off the LED 
      if (distance & 1 == 1)
        leds[i].on();
      else
        leds[i].off();
      // shift the distance value by one (same thing as dividing distance by 2)
      distance = distance >> 1;
    }
  }
}
