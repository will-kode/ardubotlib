#include <LED.h>
#include <PingSensor.h>

/**
 * Description: This program uses an ultrasonic sensor and
 *              two LEDs to signal when an object is too close
 *              to the system (in front of the ultrasonic sensor).
 * Iteration #: 1
 * Controller:  N/A
 * 
 * Components:
 *    - 1x Arduino UNO
 *    - 1x Ultrasonic sensor
 *    - 1x Red LED
 *    - 1x Green LED
**/

// the ultrasonic and LED variables
PingSensor sonar;
LED redLed, greenLed;

void setup() {
  // init serial communication (to get messages from the Arduino board)
  Serial.begin(9600);

  // set the pins that the ultrasonic sensor will emit and receive
  // pings on
  sonar.attach(7, 7);

  // init the LED variables that control the red and green LEDs
  redLed.attach(12, LED::PIN_DIGITAL);
  greenLed.attach(13, LED::PIN_DIGITAL);

  // make sure all LEDs are off
  redLed.off();
  greenLed.off();
}

void loop() {
  long duration, inches;

  // send a ultrasonic pulse and store the time it took the signal to travel
  // from the sensor to the nearest object in front of it.
  duration = sonar.ping();

  // convert the time into distance
  inches = ParaPing::microsecsToIn(duration);
  
  // print the number of inches the nearest object is away from the sensor
  Serial.print(inches);
  Serial.print("in; ");
  
  // control the LEDs
  controlLEDs(inches);
  Serial.println();
  
  delay(100);
}

void controlLEDs(long inches) {
  // used to determine when to alternate between the red and green LEDs
  const int kMinSafeDistance = 7;
  
  // if the number of inches is greater than or equal to "kMinSafeDistance",
  // then turn on the green LED (and turn off the red LED)
  if (inches >= kMinSafeDistance) {
    if (greenLed.isOn() == false) {
      redLed.off();
      greenLed.on();
      Serial.print("Green ON (it is safe)");
    }
  }
  // if the number of inches is the number of inches is below
  // "kMinSafeDistance", so turn on the red LED (and turn off the green LED)
  else {
    if (redLed.isOn() == false) {
      greenLed.off();
      redLed.on();
      Serial.print("Red ON (DANGER!!!)");
    }
  }
}
