#include <LED.h>

/**
 * Description: Simple LED example where the onboard LED for the Arduino
 *              UNO blinks (turns off and on) in intervals of 1 second.
 * Iteration #: 1
 * Controller:  N/A
 * 
 * Components:
 *    - 1x Arduino UNO
**/

// declare an "LED" variable
LED led;

void setup() {
  // use this object to control the onboard LED
  // for the Arduino UNO
  led.attach(13, LED::PIN_DIGITAL);
}

void loop() {
  // turn on the LED and then pause for 1 second
  led.on();
  delay(1000);
  // turn off the LED and then pause for 1 second
  led.off();
  delay(1000);
}
