#include <PingSensor.h>
#include <MakeItMotorShield.h>

/**
 * Description: The robot moves forward if there is no obstacle 10 inches or
 *              more in front of it. If there is an obstacle less than 10
 *              inches in front of the robot, it halts.
 * Iteration #: 1
 * Controller:  Reactive
 * 
 * Components:
 *    - 1x Arduino UNO
 *    - 1x Ultrasonic sensor
 *    - 1x Make:it Robotics Starter Kit Motor Shield
 *      - 1x Motor shield
 *      - 2x DC motors
 *      - 2x Wheels
**/

PingSensor sonar;               // used to control ultrasonic sensor
MakeItMotorShield mShield;      // used to control motor shield
const uint8_t kLeftMotorId = 3; // port number for left motor
const uint8_t kRghtMotorId = 1; // port number for right motor

void setup() {
  // initialize the ping sensor
  sonar.attach(7, 7);

  // initialize the motor shield
  mShield.begin();
  mShield.activatePort(kLeftMotorId);
  mShield.activatePort(kRghtMotorId);
}

void loop() {
  // get the distance in inches
  unsigned long duration, distance;
  duration = sonar.ping();
  distance = PingSensor::microsecsToIn(duration);

  if (distance >= 10) { // continue moving forward
    mShield.motorWrite(kRghtMotorId, 200);
    mShield.motorWrite(kLeftMotorId, 200);
  } else { // distance is less then 10 inches
    mShield.motorWrite(kRghtMotorId, 0);
    mShield.motorWrite(kLeftMotorId, 0);
  }
  // perform this "loop" 5 times a second
  delay(200);
}
