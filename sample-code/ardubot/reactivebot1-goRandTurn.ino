#include <PingSensor.h>
#include <MakeItMotorShield.h>

/**
 * Description: The robot moves forward if there is no obstacle 10 inches or
 *              more in front of it. If there is an obstacle less than 10
 *              inches in front of the robot, it turns in a random direction
 *              until there is nothing within 10 inches in front of it.
 * Iteration #: 2
 * Controller:  Reactive
 * 
 * Components:
 *    - 1x Arduino UNO
 *    - 1x Ultrasonic sensor
 *    - 1x Make:it Robotics Starter Kit Motor Shield
 *      - 1x Motor shield
 *      - 2x DC motors
 *      - 2x Wheels
**/

PingSensor sonar;               // used to control ultrasonic sensor
MakeItMotorShield mShield;      // used to control motor shield
const uint8_t kLeftMotorId = 3; // port number for left motor
const uint8_t kRghtMotorId = 1; // port number for right motor
const int16_t kMotorSpeed = 200;// the speed of the motor
boolean isTurning = false;      // used to determine if the robot is turning

void setup() {
  // initialize the ultrasonic sensor
  sonar.attach(7, 7);

  // initialize the motor shield
  mShield.begin();
  mShield.activatePort(kLeftMotorId);
  mShield.activatePort(kRghtMotorId);
  
  // "seed" the random number generator with signal "noise"
  randomSeed(analogRead(0));
}

void loop() {
  // get the distance in inches
  unsigned long duration, distance;
  duration = sonar.ping();
  distance = PingSensor::microsecsToIn(duration);

  if (distance >= 10) { // continue moving forward
    mShield.motorWrite(kRghtMotorId, kMotorSpeed);
    mShield.motorWrite(kLeftMotorId, kMotorSpeed);
    isTurning = false;
  } else { // distance is less then 10 inches
    // if the robot is not already turning, do the following
    if (isTurning == false) {
      // get a random number of either 0 or 1
      // - if the number is 0, turn left
      // - else, turn right
      long randNumber = random(0, 2);
      if (randNumber == 0) {
        // to turn left, reverse the left wheel as the right
        // wheel turn forward
        mShield.motorWrite(kRghtMotorId,  kMotorSpeed);
        mShield.motorWrite(kLeftMotorId, -kMotorSpeed);
      } else {
        // to turn right, reverse the right wheel as the left
        // wheel turn forward
        mShield.motorWrite(kRghtMotorId, -kMotorSpeed);
        mShield.motorWrite(kLeftMotorId,  kMotorSpeed);
      }
      isTurning = true;
    }
  }
  // perform this "loop" 5 times a second
  delay(200);
}
