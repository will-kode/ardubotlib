#include <MakeItMotorShield.h>

/**
 * Description: A simple program that continually moves
 *              forward and turns the robot's body for
 *              4 seconds.
 * Iteration #: 1
 * Controller:  N/A
 * 
 * Components:
 *    - 1x Arduino UNO
 *    - 1x Make:it Robotics Starter Kit Motor Shield
 *      - 1x Motor shield
 *      - 2x DC motors
 *      - 2x Wheels
**/

// a MakeItMotorShield object, which will control the motors
MakeItMotorShield shield;

void setup() {
  // initialize the motor shield object
  shield.begin();
  // activate ports 1 and 3
  shield.activatePort(1);
  shield.activatePort(3);
}

void loop() {
  // move forward at full speed for 4 seconds
  shield.motorWrite(1, 255);
  shield.motorWrite(3, 255);
  delay(4000);
  // turn for 4 seconds
  shield.motorWrite(1, -255);
  shield.motorWrite(3, 255);
  delay(4000);
}
